## Contact

 If you find a bug, please post it as an issue on the project page. (Login required)

  * [RealtimeSearch (Bitbucket)](https://bitbucket.org/neelabo/realtimesearch)
 
 You can also report back to us in the comments of this blog.

  * [ヨクアルナニカ](https://yokuarunanika.blogspot.com/)
 
Email

  * [nee.laboratory@gmail.com](mailto:nee.laboratory@gmail.com)