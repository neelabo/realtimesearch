## Environment

  * Windows 10, 64bit

## How to install/uninstall

### ZIP version

- RealtimeSearch4.0.zip

  No installation required, just extract the ZIP and run `RealtimeSearch.exe`.  
  To uninstall, simply delete the entire folder.

### ZIP version  (framework dependent)

- RealtimeSearch4.0-fd.zip

  Same as the ZIP version.
  "NET 9.0 Desktop Runtime - Windows x64” is required to run the software. Follow the startup message to download and install the software from the Microsoft page.

### Installer version

- RealtimeSearch4.0.msi

  Run the MSI installer and follow its instructions to install.
  Uninstallation is also performed using standard OS methods.

## Quick Start

  First, add folders to be searched in the settings. 
  Enter search terms in the text box at the top of the main window to start the search.
  Various operations can be performed from the context menu of the search result files.


