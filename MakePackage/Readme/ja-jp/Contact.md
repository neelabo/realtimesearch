## 連絡先

 不具合を見つけたらプロジェクト ページに課題として投稿してください。 (ログインが必要です)

  * [RealtimeSearch (Bitbucket)](https://bitbucket.org/neelabo/realtimesearch)
 
 こちらのブログのコメントでもご報告いただけます。

  * [ヨクアルナニカ](https://yokuarunanika.blogspot.com/)
 
メールでの連絡先

  * [nee.laboratory@gmail.com](mailto:nee.laboratory@gmail.com)